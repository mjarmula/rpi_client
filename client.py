import time
import json
import configparser
from lib.dependency_injection import DependencyInjection
from lib.rpi import RPI
from lib.websocket_manager import WebsocketManager
from lib.event_manager import EventManager


class RPIClient:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')
        di = DependencyInjection()
        di.register(
            'event_manager',
            EventManager()
        ).register('rpi', RPI()).register('config', config)
        self.wm = WebsocketManager(
            di,
            config['DEFAULT']['host'],
            config['DEFAULT']['port']
        )


if __name__ == "__main__":
    rpic = RPIClient()
    rpic.wm.ws.run_forever()
