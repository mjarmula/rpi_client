from random import randint
import time
from .message import Message
from .observer_manager import ObserverManager
import sys
import Adafruit_DHT
import subprocess

class RPI:
    def __init__(self):
        self.temperature = None
        self.humidity = None
        ObserverManager(self)

    def deamon(self):
        while(True):
            time.sleep(10)
            self.check_temperature()
            self.check_humidity()

    def check_temperature(self):
        id_sensor = 1
        sensor = Adafruit_DHT.DHT22
        _, current_temperature = Adafruit_DHT.read_retry(sensor, 20)
        current_temperature = '{:3.1f}'.format(current_temperature)
        self.check('temperature', current_temperature, id_sensor)

    def check_humidity(self):
        current_humidity = randint(200, 255)
        id_sensor = 2
        sensor = Adafruit_DHT.DHT22
        current_humidity, _ = Adafruit_DHT.read_retry(sensor, 20)
        current_humidity = '{:2.0f}'.format(current_humidity)
        self.check('humidity', current_humidity, id_sensor)

    def check(self, old_val, new_val, id_sensor):
        parsed_old_val = getattr(self, old_val)
        if(not parsed_old_val or parsed_old_val != new_val):
          body = dict(value=new_val, type=old_val, id_sensor=id_sensor)
          msg = Message("report", body).to_json()
          setattr(self, old_val, new_val)
          self.em.trigger('report', msg)
