class DependencyInjection:
    def __init__(self):
        self.dependencies = {}
    def register(self, name, obj):
        self.dependencies[name] = obj
        return self
    def get(self, name):
        return self.dependencies[name]
