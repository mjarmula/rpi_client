import json

class Message:
    def __init__(self, message_type, val=None):
        self.type = message_type
        self.val = val
    def to_json(self):
        return json.dumps({
            'event': self.type,
            'data': self.val
        })
