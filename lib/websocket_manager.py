import websocket
import _thread as thread
from .message import Message


class WebsocketManager:
    def __init__(self, dependency_injection, host, port):
        websocket.enableTrace(False)
        self.ws = websocket.WebSocketApp("ws://{}:{}".format(host, port),
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)
        self.ws.on_open = self.on_open
        self.config = dependency_injection.get('config')
        self.em = dependency_injection.get('event_manager')
        self.rpi = dependency_injection.get('rpi')
        self.rpi.em = self.em
        self.em.add_event_listener('report', lambda data: self.ws.send(data))

    def on_message(self, ws, message):
        print(message)

    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws):
        print("### closed ###")

    def on_open(self, ws):
        def run(*args):
            self.ws.send(Message('join_channel', {
                'client_id': self.config['DEFAULT']['user_id']
            }).to_json())
            self.rpi.deamon()

        thread.start_new_thread(run, ())
