class Registry:
    collection = {}

    @staticmethod
    def register(name, obj):
        Registry.collection[name] = obj
    def get(name):
        return Registry.collection[name]
